#!/bin/bash

if [ -d /sys/class/hwmon/hwmon4 ]; then
    echo "INA219 found"
    exit 1
fi

echo ina219 0x41 > /sys/bus/i2c/devices/i2c-7/new_device
