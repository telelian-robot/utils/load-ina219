#!/bin/bash

TARGET=load-ina219

sudo mkdir -p /etc/$TARGET

# cp executors
sudo cp ./enable_ina219.sh /etc/$TARGET
# cp service
sudo cp ./$TARGET.service /etc/systemd/system/

# register
sudo systemctl enable $TARGET
sudo systemctl start $TARGET
